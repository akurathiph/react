#!/bin/sh
export PATH=$PATH:/usr/local/bin
cd .
/usr/local/bin/npm install
/usr/local/bin/npm run build
cp -r build /home/centos/apache-tomcat-8.0.21/webapps/
cd /home/centos/apache-tomcat-8.0.21/webapps/
rm -rf B2BNext 
mv build B2BNext	
cd /home/centos/apache-tomcat-8.0.21/bin/
sh shutdown.sh
sh startup.sh